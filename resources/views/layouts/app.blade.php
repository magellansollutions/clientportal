<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>{{ config('app.name', 'Magellan Solution Client Portal') }}</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	  <?php 
	  $asset = URL::asset('/'); 
	  $today = getdate(); ?>
	  <script>
    var d = new Date(<?php echo time(); ?>*1000);
   // var d = new Date(Date.UTC(<?php echo $today['year'].",".$today['mon'].",".$today['mday'].",".$today['hours'].",".$today['minutes'].",".$today['seconds']; ?>));
    setInterval(function() {
        d.setSeconds(d.getSeconds() + 1);
        $('#timer').text((d.getHours() +':' + d.getMinutes() + ':' + d.getSeconds() ));
    }, 1000);
	
</script> 
	
	
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">




    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('plugin/jquery-ui-1.12.1.custom/jquery-ui.css') }}" rel="stylesheet">
	<link href="{{ asset('plugin/jquery-ui-1.12.1.custom/jquery-ui.theme.css') }}" rel="stylesheet">
	<link href="{{ asset('plugin/jquery-ui-1.12.1.custom/jquery-ui.structure.css') }}" rel="stylesheet">
	<link href="{{ asset('plugin/jquery-ui-1.12.1.custom/jquery.datetimepicker.css') }}" rel="stylesheet">
	<link rel="stylesheet" href="{{$asset}}inventory/css/bootstrap.css">
	<link rel="stylesheet" href="{{$asset}}inventory/datatables/jquery.dataTables.min.css">
	<link rel="stylesheet" href="{{$asset}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/css/buttons.dataTables.css">

	
	
	    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
	<script src="{{ asset('js/customescript.js') }}"></script>
	<script src="{{ asset('plugin/jquery-ui-1.12.1.custom/jquery-ui.js') }}"></script>
	<script src="{{ asset('plugin/jquery-ui-1.12.1.custom/jquery.datetimepicker.full.js') }}"></script>
	
	
	<script src="{{$asset}}inventory/datatables/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>
	<script src="{{$asset}}inventory/datatables/jquery.dataTables.yadcf.js"></script>

	<script src="{{$asset}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js"></script>
	<script src="{{$asset}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js"></script>
	<script src="{{$asset}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/jszip/dist/jszip.min.js"></script>
	<script src="{{$asset}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js"></script>
	<script src="{{$asset}}inventory/datatables/DataTables-1.10.15/extensions/Buttons/js/buttons.colVis.min.js"></script>
	
	
	@yield('header-scripts')
	
	
	
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
						<img src = "{{$asset}}upload/images/Magellan_Solutions_logo.png" class="header-logo"/>
                        <?php //{{ config('app.name', 'Laravel') }} TEST {{$asset }} */ ?>
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                  

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
					<li style="margin-top:12px;">Manila Time - <label id="timer" style="font-size:15px;font-family:DigifaceWide;text-shadow: 1px 1px #000000;letter-spacing: 2px;"></label></li>
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <?php /* <!-- <li><a href="{{ route('register') }}">Register</a></li> --> */?>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
		 @guest
		 @else
		<div class="sidenavigation" style="float:left">
				<ul class="nav in" id="side-menu">
                        
                        <li class="&quot;active&quot;">
                            <a href="{{ asset('clientview/') }}" class="active"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('clienview.calls')}}"><i class="fa fa-bar-chart-o fa-fw"></i> Calls</a>
								<ul class="sub-menu collapse in" aria-expanded="true" style="">
									<li>
										<a href="{{$asset}}clientview/calls/index?source=shared">
											<i class="fa fa-fw fa-list-ol"></i>Shared
										</a>
									</li>            
									<li>
										<a href="{{$asset}}clientview/calls/index?source=dedicated">
											<i class="fa fa-fw fa-list-ol"></i>Dedicated
										</a>
									</li> 									
								</ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-table fa-fw"></i> EOD</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-edit fa-fw"></i> Recordings</a>
                        </li>
                        
                    </ul>
						
		
		
		</div>
		 @endguest
        <div class="wrapper-container">
		@yield('content')
		</div>
		<div style="clear:both"></div>
    </div>

@yield('footer-scripts')
</body>
</html>
