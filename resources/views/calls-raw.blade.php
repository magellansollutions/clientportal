<?php $asset = URL::asset('/'); ?> 
@extends('layouts.app')

@section('title', 'index')

@section('header-scripts')

<link rel="stylesheet" type="text/css" href="{{$asset}}timepicker2/jquery.datetimepicker.css"/>

 <?php $campaign = array();

 $campaign = explode(",",Auth::user()->campaign_previlege); ?>

@endsection

@section('content')
<div class="container-fluid">

    <div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="panel panel-default">
						<form method="get" action="{{route('clienview.calls')}}" class="form-horizontal p-10">
										{{ csrf_field() }}            
								<div class="panel-heading">Generate Call Records (raw data) <?php  if(isset($_GET['source']) && $_GET['source']!='') {echo $_GET['source'];} ?>  <input type="hidden" name="source" value="<?php  if(isset($_GET['source']) && $_GET['source']!='') {echo $_GET['source'];} ?>"/><!--<span class=""><select readonly name="source" style="
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;"> <option value="shared">Shared </option><option value="dedicated">Dedicated</option></select></span> -->
									<hr/>
										<div class="row">
											<div class="col-md-3">
													<div class="form-group p-10">
														<label class="control-label col-md-2" for="text">From:
														</label>
														<div class="col-md-10">
															<input type="text" class="form-control datetimepicker" value="<?php  if(isset($_GET['from']) && $_GET['from']!='') {echo $_GET['from'];} ?>" placeholder="from" name="from">
														</div>
													</div>
											</div>
											<div class="col-md-3">
													<div class="form-group p-10">
														<label class="control-label col-md-2" for="text">To:
														</label>
														<div class="col-md-10">
															<input type="text" class="form-control datetimepicker" value="<?php  if(isset($_GET['to']) && $_GET['to']!='') {echo $_GET['to'];} ?>"  placeholder="to" name="to">
														</div>
													</div>
											</div>
											<div class="col-md-4">
												<div class="form-group p-10">
													<label class="col-md-3 control-label" for="example-select">Select:</label>
													<div class="col-md-9">
														<select name="camplist" class="form-control" size="1">

																@foreach( $campaign as $val)
																<option value="{{$val}}" <?php  if(isset($_GET['camplist']) && $_GET['camplist']!='') {echo 'selected';} ?>>{{$val}}</option>
																@endforeach
														</select>
													</div>
												</div>
											</div> 
											<div class="col-md-2">
												<div class="form-group p-10 form-actions">
													<div class="col-md-12">
														<button type="submit" class="btn btn-primary">Submit
														</button>
													</div>
												</div>
											</div>               
										</div>
								</div>
						</form>
					</div>
	
               
               
					<div class="panel panel-default">
							<div class="panel-heading">
							
							
										
										@if(isset($filtered_data) || isset($filtered_data_out))
							<div class="panel-body report-data">
															  <?php 
																	$new_filtered_data = json_decode($filtered_data);
																		if(isset($filtered_data_out)){
																		$new_filtered_data_out = json_decode($filtered_data_out) ;
																		}
																	
																	//}?>
														<div class="row">
															  <div class="col-md-12">
															  <?php echo count($new_filtered_data) . " Records Found" ?>
																	
															  </div>
														  </div>
																	

															  <table class="table table-bordered table-striped" id="tabledrecords" >
																  <thead>
																  <tr>
																	  <th>Agent Name</th>
																	  <th>Call Date</th>
																	  <th>Local Number</th>
																	  <th>Remote Number</th>
																	  <th>Campaign (Ingroup Name)</th>
																	  <th>Disposition</th>
																	  <th>Call Direction</th>
																	  <th>Duration (sec)</th>
																	  <th>Queue(ms)</th>
																  </tr>
																  </thead>
																  <tbody>
																	
																  
																   <?php foreach($new_filtered_data as $rows => $data_records ) {?>
																	<?php if($source=="shared") {?>
																	<tr>
																   <td nowrap> {{$data_records->localuserid}}</td>
																		  <td nowrap>{{$data_records->connecteddate}}</td>
																		  <td>{{$data_records->dnis}}</td>
																		  <td>{{$data_records->remotenumber}}</td>
																		  <td>{{$data_records->assignedworkgroup}}</td>
																		  <td>{{$data_records->wrapupcode}}</td>
																		  <td>{{$data_records->calldirection}}</td>
																		  <td>{{$data_records->calldurationseconds}}</td>
																		  <td>{{$data_records->tqueuewait}}</td>
																	  </tr>
																	<?php }
																	else if($source=="dedicated"){?>
																	<tr>
																   <td nowrap> {{$data_records->user}}</td>
																		  <td nowrap>{{$data_records->call_date}}</td>
																		  <td>{{$data_records->phone_number}}</td>
																		  <td>{{$data_records->phone_number}}</td>
																		  <td>{{$data_records->campaign_id}}</td>
																		  <td>{{$data_records->status}}</td>
																		  <td>Inbound</td>
																		  <td>{{$data_records->length_in_sec}}</td>
																		  <td>{{$data_records->queue_seconds}}</td>
																	  </tr>
																
																	<?php } ?>
																   <?php 
																   } 
																   
																   
																   
																   /* for($x =1 ; $x<10; $x++) {?>
																	  <tr>
																		  <td nowrap>Ruth Villanueva </td>
																		  <td nowrap>2017-12-21 00:32:21.450</td>
																		  <td>+13462230001</td>
																		  <td>sip:nocmarlon@192.168.200.91</td>
																		  <td>OmniBookMe</td>
																		  <td>Test Call</td>
																		  <td>Inbound</td>
																		  <td>14</td>
																		  <td>153</td>
																	  </tr>
																	<?php } */?>
																												
																	  
																											</tbody>
															  </table> 

																													
							 </div>
							 @endif

							
						</div>
					
					</div>
				
				
				
				
    </div>
			
		
			
 </div>
@endsection

@section('footer-scripts')

<script src="{{$asset}}timepicker2/build/jquery.datetimepicker.full.js"></script>
<script type="text/javascript">
var date = $('.datetimepicker').datetimepicker({
  timeFormat: 'HH:mm:ss z'
});

$(document).ready(function() {
    /* $('#tabledrecords').DataTable(
	 {
       
       
        dom: 'Bfrtip',
        buttons: [
            'copy', 'excel', 'print'
        ]
        });
		
		} ); */
		
	$('#tabledrecords').DataTable({
        lengthMenu: [[10, 25, 50, 75, 100, -1], [10,25, 50, 75, 100, "All"]],
        pageLength: 215, 
        scrollX: true,
        autoWidth: false,
        responsive: true,
        
        scrollCollapse: true,
        dom: 'Blfrtip',
        stateSave: true,
        buttons: [
            {
                extend: 'copyHtml5',
                title: "Reports" + " " + d,
                exportOptions: {
                    columns: [ ':visible:not(.not-export-col)' ]
                }
            },
            {
                extend: 'excelHtml5',
                title: "Reports" + " " + d,
                exportOptions: {
                    columns: [ ':visible:not(.not-export-col)' ]
                }
            },
            'colvis'
            ]
            });
	
		
			} ); 
		
		

</script>    
@endsection
