<?php namespace App\Http\Controllers\clientview;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

use AppHelper;


class indexController extends Controller
{

	public function index(){
		
		
		//echo "This is a test";
		return view("dashboard");
		//return view('bloodstone.dashboard.index',compact('items','users','help','type','larr_myarray','count','userlist','qrcode'));

	}
	
		public function callsrawdata(Request $r){
			
			$source = $r->source;
			if($source == 'shared')
			{
			$DropdownCampaigns = new \App\Model\clientview\JasperIcbm;
				if(isset($r->camplist)){
						$this->validate($r,[
							'from' => 'required',
							'to' => 'required'
						]);
						
				//
				
				$filtered_data = $DropdownCampaigns->where('assignedworkgroup','=',$r->camplist)->whereBetween('ConnectedDate',[$r->from,$r->to])->get();
				//$filtered_data = $DropdownCampaigns->whereBetween('connecteddate',['2017-12-21 21:29:35.370','2017-12-20 21:29:35.370'])->get();
			
				return view("calls-raw")->with('filtered_data',$filtered_data)->with('source',$source);
				}
				else{
					return view("calls-raw");
				}
			}
			else if($source == 'dedicated')
			{
				$DropdownCampaigns = new \App\Model\clientview\JasperViciIn;
				$DropdownCampaignsOut = new \App\Model\clientview\JasperViciOut;
					if(isset($r->camplist)){
							$this->validate($r,[
								'from' => 'required',
								'to' => 'required'
							]);
							
					//
					
					$filtered_data = $DropdownCampaigns->where('campaign_id','=',$r->camplist)->whereBetween('call_date',[$r->from,$r->to])->get();
					$filtered_data_out = $DropdownCampaignsOut->where('campaign_id','=',$r->camplist)->whereBetween('call_date',[$r->from,$r->to])->get();
					//$filtered_data = $DropdownCampaigns->whereBetween('connecteddate',['2017-12-21 21:29:35.370','2017-12-20 21:29:35.370'])->get();
				
					return view("calls-raw")->with('filtered_data',$filtered_data)->with('source',$source)->with('filtered_data_out',$filtered_data_out);
					}
					else{
						return view("calls-raw");
					}
				
				
				
				
				
				var_dump($DropdownCampaigns);
				
			}
			else
			{
					return view("calls-raw"); // RETURN EMPTY RECORD 
				
			}
		
	}
	
	
	
	
	

}