<?php namespace App\Model\Clientview;

use Illuminate\Database\Eloquent\Model;
use Auth;

class JasperViciOut extends Model
{

	protected $connection =  'client_portal';
	protected $table = 'call_log';
    protected $fillable = ['uniqueid','lead_id','list_id','campaign_id','call_date','start_epoch','end_epoch','length_in_sec','status','phone_code','phone_number','user','comment','processed','user_group','term_reason','alt_dial','called_count']; 
   
    
	

}