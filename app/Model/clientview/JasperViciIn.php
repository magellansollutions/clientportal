<?php namespace App\Model\Clientview;

use Illuminate\Database\Eloquent\Model;
use Auth;

class JasperViciIn extends Model
{

	protected $connection =  'client_portal';
    protected $table = 'closer_log';
    protected $fillable = ['closecallid','lead_id','list_id','campaign_id','call_date','start_epoch','end_epoch','length_in_sec','status','phone_code','phone_number','user','comment','processed','queue_seconds','user_group','xfercallid','term_reason','uniqueid','agent_only','queue_position','called_count']; 

}